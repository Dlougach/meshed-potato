find_package(Boost 1.40 COMPONENTS program_options system filesystem REQUIRED)

file(GLOB SOURCE_FILES
        *.h
        *.cc)

add_executable(meshgen_cli ${SOURCE_FILES})

target_link_libraries(meshgen_cli PRIVATE 2d Boost::program_options Boost::system Boost::filesystem)
