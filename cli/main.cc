#include <boost/exception/all.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <map>
#include <2d/delaunay.h>
#include <2d/point_deduplicator.h>

using std::string;
namespace po = boost::program_options;
namespace fs = boost::filesystem;

namespace meshgen {
namespace {

class InputFormatError : public std::runtime_error {
  using std::runtime_error::runtime_error;
};

class Application {
 public:
  Application() {
    points_counter_.reset(new lib2d::PointsCounter(triangulation_.mutable_points()));
  }

  void ParseCommandLine(int argc, char **argv) {
    po::options_description desc{"Options", 110, 80};
    desc.add_options()
        ("help,h", "Help screen")
        ("input", po::value<string>()->required()->value_name("FILE"),
         "Triangulation input data in the following format:\n"
         "  [default size bound]\n"
         "  [x_min_1] [y_min_1] [x_max_1] [y_max_1] [size_bound_1]"
         "      \t(stricter size bounds in a domain)\n"
         "  [x_min_2] [y_min_2] [x_max_2] [y_max_2] [size_bound_2]\n"
         "  . . .\n"
         "  [x_min_k] [y_min_k] [x_max_k] [y_max_k] [size_bound_k]\n"
         "                     (empty line)\n"
         "  [N1]\n"
         "  [x_1] [y_1]\n"
         "  . . .              (N1 points)\n"
         "  [x_N1] [y_N1] \n"
         "  [N2] \n"
         "  [x_1] [y_1]\n"
         "  . . .              (N2 points)\n"
         "  [x_N2] [y_N2] \n"
         "  (etc.)")
        ("grid", po::value<string>()->default_value("")->value_name("FILE"), "Path to fort.30 file")
        ("output", po::value<string>()->required()->value_name("FILE_PREFIX"),
         "Output file prefix. Actual output files will have names:\n"
         "FILE_PREFIX.nodes, FILE_PREFIX.elemente, FILE_PREFIX.neighbor");

    po::variables_map vm;

    store(parse_command_line(argc, argv, desc), vm);

    if (vm.count("help")) {
      std::cout << desc << std::endl;
      std::exit(0);
    }

    notify(vm);

    input_path_ = vm["input"].as<string>();
    if (!is_regular_file(input_path_)) {
      std::cerr << "File " << input_path_ << " is either missing or not a regular file." << std::endl;
      std::exit(1);
    }
    grid_path_ = vm["grid"].as<string>();
    if (!grid_path_.empty() && !is_regular_file(grid_path_)) {
      std::cerr << "File " << grid_path_ << " is either missing or not a regular file." << std::endl;
      std::exit(1);
    }
    output_nodes_path_ = output_elements_path_ = output_neighbors_path_ = vm["output"].as<string>();
    output_nodes_path_.concat(".nodes");
    output_elements_path_.concat(".elemente");
    output_neighbors_path_.concat(".neighbor");
  }

  void ReadInput() {
    fs::ifstream input(input_path_);
    int current_line_index = 0;
    auto next_line = [&]() {
      string line;
      getline(input, line);
      ++current_line_index;
      return Tokenize(line);
    };
	static const double kScale = 10.0;

    auto line = EnsureSize(next_line(), 1);
    triangulation_params_.add_criteria()->set_max_side_length(static_cast<float>(ParseDouble(line[0]) * kScale));
    for (;;) {
      line = next_line();
      if (line.empty())
        break;
      EnsureSize(line, 5);
      auto *criterion = triangulation_params_.add_criteria();
      criterion->mutable_domain()->set_left(ParseDouble(line[0]) * kScale);
      criterion->mutable_domain()->set_top(ParseDouble(line[1]) * kScale);
      criterion->mutable_domain()->set_right(ParseDouble(line[2]) * kScale);
      criterion->mutable_domain()->set_bottom(ParseDouble(line[3]) * kScale);
      criterion->set_max_side_length(static_cast<float>(ParseDouble(line[4]) * kScale));
    }
    while (!input.eof()) {
      line = next_line();
      if (line.empty())
        continue;
      EnsureSize(line, 1);
      int N = ParseInt(line[0]);
      auto *boundary = triangulation_.add_boundaries();
      for (int i = 0; i < N; ++i) {
        line = EnsureSize(next_line(), 2);
        lib2d::Point p;
        p.set_x(ParseDouble(line[0]) * kScale);
        p.set_y(ParseDouble(line[1]) * kScale);
        boundary->add_point_indices(points_counter_->GetIndex(p));
      }
    }
  }

  void ReadGrid() {
    if (grid_path_.empty())
      return;
    fs::ifstream input(grid_path_);
    int current_line_index = 0;
    auto next_line = [&]() {
      string line;
      getline(input, line);
      ++current_line_index;
      return Tokenize(line);
    };
    for (int i = 0; i < 3; ++i)
      next_line();  // Skip useless part of the header.
    auto header = EnsureSize(next_line(), 3);
    int columns = ParseInt(header[0]), rows = ParseInt(header[1]), breaks = ParseInt(header[2]);
    for (int i = 0; i < breaks; ++i)  // Is it so?
      next_line();
    next_line();
	static const double kScale = 1000.0;
    for (int c = 0; c < columns; ++c) {
      for (int r = 0; r < rows; ++r) {
        auto xs = EnsureSize(next_line(), 4);
        auto ys = EnsureSize(next_line(), 4);
        lib2d::Triangulation::Grid::Cell* cell = triangulation_.mutable_grid()->add_cells();
        cell->mutable_id()->set_column_index(c + 1);
        cell->mutable_id()->set_row_index(r + 1);
        for (int i = 0; i < 4; ++i) {
          lib2d::Point p;
          p.set_x(ParseDouble(xs[i]) * kScale);
          p.set_y(ParseDouble(ys[i]) * kScale);
          cell->mutable_border()->add_point_indices(points_counter_->GetIndex(p));
        }
      }
    }
  }

  void Triangulate() {
    points_counter_ = nullptr;
    lib2d::CreateDelaunayTriangulator()->Do(&triangulation_, triangulation_params_);
  }

  void WriteOutput() {
    WriteNodes();
    WriteElements();
    WriteNeighbors();
  }

 private:
  static std::vector<string> EnsureSize(std::vector<string> v, int s) {
    if (size_t(s) != v.size()) {
      throw InputFormatError("Input is invalid");
    }
    return v;
  }

  static std::vector<string> Tokenize(const string &s) {
    std::istringstream iss(s);
    std::vector<string> result;
    for (string tok; iss >> tok;) {
      result.push_back(tok);
    }
    return result;
  }

  static double ParseDouble(const string &s) {
    std::istringstream iss(s);
    double res;
    if ((iss >> res).fail())
      throw InputFormatError("Couldn't read double: " + s);
    return res;
  }

  static int ParseInt(const string &s) {
    std::istringstream iss(s);
    int res;
    if ((iss >> res).fail())
      throw InputFormatError("Couldn't read integer: " + s);
    return res;
  }

  void WriteNodes() {
    fs::ofstream out(output_nodes_path_);
    int n = triangulation_.points_size();
    out << n;
    out << std::scientific << std::fixed << std::setprecision(6);
    for (int i = 0; i < n; ++i) {
      if (i % 4 == 0)
        out << std::endl;
      out << std::setw(16) << triangulation_.points(i).x();
    }
    for (int i = 0; i < n; ++i) {
      if (i % 4 == 0)
        out << std::endl;
      out << std::setw(16) << triangulation_.points(i).y();
    }
    out << std::endl;
  }

  void WriteElements() {
    fs::ofstream out(output_elements_path_);
    const auto &triangles = triangulation_.triangles();
    out << triangles.size() << std::endl;
    out << std::fixed;
    for (int i = 0; i < triangles.size(); ++i) {
      out << std::setw(7) << i + 1;
      for (int ind: triangles.Get(i).border().point_indices()) {
        out << std::setw(7) << ind + 1;
      }
      out << std::endl;
    }
  }

  void WriteNeighbors() {
    fs::ofstream out(output_neighbors_path_);
    using Edge = std::pair<int, int>;
    struct OwnerDescription {
      int triangle_index;
      int edge_index;
    };
    std::map<Edge, OwnerDescription> mp;
    for (int i = 0; i < triangulation_.triangles_size(); ++i) {
      const auto &triangle = triangulation_.triangles(i);
      for (int j = 0; j < 3; ++j) {
        Edge e = {triangle.border().point_indices(j), triangle.border().point_indices((j + 1) % 3)};
        OwnerDescription od = {i + 1, j + 1};
        if (!mp.insert(std::make_pair(e, od)).second) {
          throw std::logic_error("Same edge encountered (in same direction) found in 2 triangles.");
        }
      }
    }
	out << triangulation_.triangles_size() << std::endl;
    for (int i = 0; i < triangulation_.triangles_size(); ++i) {
      out << std::setw(7) << i + 1;
      const auto &triangle = triangulation_.triangles(i);
      for (int j = 0; j < 3; ++j) {
        Edge reverse_edge = {triangle.border().point_indices((j + 1) % 3), triangle.border().point_indices(j)};
        auto it = mp.find(reverse_edge);
        OwnerDescription neighbor = {0, 0};
        if (it != mp.end()) {
          neighbor = it->second;
        }
        out << std::setw(7) << neighbor.triangle_index;
        out << std::setw(7) << neighbor.edge_index;
        out << std::setw(7) << 0;
      }
      if (triangle.has_cell_id()) {
        out << std::setw(7) << triangle.cell_id().column_index() << std::setw(7) << triangle.cell_id().row_index();
      } else {
        out << std::setw(7) << -1 << std::setw(7) << -1;
      }
      out << std::endl;
    }
  }

 private:
  fs::path input_path_, grid_path_, output_nodes_path_, output_elements_path_, output_neighbors_path_;
  lib2d::Triangulation triangulation_;
  std::unique_ptr<lib2d::PointsCounter> points_counter_;
  lib2d::TriangulationParams triangulation_params_;
};

}  // namespace
}  // namespace meshgen

int main(int argc, char **argv) {
  try {
    meshgen::Application app;
    app.ParseCommandLine(argc, argv);
    app.ReadInput();
    app.ReadGrid();
    std::cout << "Input successfully read. Now running triangulation." << std::endl;
    app.Triangulate();
    std::cout << "Writing out the result." << std::endl;
    app.WriteOutput();
    std::cout << "Done." << std::endl;
  } catch (const std::exception &ex) {
    std::cerr << ex.what() << std::endl;
    std::exit(1);
  }
}