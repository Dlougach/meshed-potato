#include "common.h"

namespace meshgen {
namespace lib2d {

Point MakePoint(double x, double y) {
  Point result;
  result.set_x(x);
  result.set_y(y);
  return result;
}

}  // namespace lib2d
}  // namespace meshgen
