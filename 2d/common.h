#pragma once

#include <2d/proto/primitives.pb.h>

namespace meshgen {
namespace lib2d {

Point MakePoint(double x, double y);

}  // namespace lib2d
}  // namespace meshgen
