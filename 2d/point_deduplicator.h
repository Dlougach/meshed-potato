#pragma once

#include <2d/proto/primitives.pb.h>

#include <memory>

namespace meshgen {
namespace lib2d {

class PointsCounter final {
 public:
  explicit PointsCounter(::google::protobuf::RepeatedPtrField<Point>* points);
  PointsCounter(const PointsCounter&) = delete;
  PointsCounter(PointsCounter&&) = delete;
  ~PointsCounter();

  int GetIndex(const Point& p);
 private:
  class Impl;
  std::unique_ptr<Impl> impl_;
};

}  // namespace lib2d
}  // namespace meshgen
