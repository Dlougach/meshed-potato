#include "delaunay_mesher.h"

#include <boost/iterator/transform_iterator.hpp>

#include <CGAL/Arrangement_2.h>
#include <CGAL/Arr_extended_dcel.h>
#include <CGAL/Arr_landmarks_point_location.h>
#include <CGAL/Arr_polyline_traits_2.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>

namespace meshgen {
namespace lib2d {
namespace internal {

inline double Sqr(double p) {
  return p * p;
}

inline DelaunayTriangulationAlgorithm::CDT::Point AsCdtPoint(const Point &point) {
  return {point.x(), point.y()};
}

struct DelaunayTriangulationAlgorithm::Context {
  CTplus cdt;
  int next_id = 0;
  std::vector<Vertex_handle> vertex_handles;

  void RegisterPoints(const ::google::protobuf::RepeatedPtrField<Point> &points) {
    vertex_handles.reserve(vertex_handles.size() + points.size());
    for (const auto &p: points) {
      auto handle = cdt.insert(AsCdtPoint(p));
      GetVertexId(handle);
      vertex_handles.push_back(handle);
    }
  }

  int GetVertexId(const Vertex_handle &handle) {
    boost::optional<int> &info = handle->info();
    if (!info) {
      info = next_id;
      ++next_id;
    }
    return info.get();
  }

  void ExportAddedPoints(::google::protobuf::RepeatedPtrField<Point> *points) {
    boost::unordered_set<int> unused_indices;
    for (int i = 0; i < next_id; ++i) {
      unused_indices.insert(i);
    }
    for (auto vertices_iter = cdt.finite_vertices_begin(); vertices_iter != cdt.finite_vertices_end();
         ++vertices_iter) {
      int id = GetVertexId(vertices_iter);
      unused_indices.erase(id);
      while (points->size() <= id) {
        points->Add();
      }
      auto *p = points->mutable_data()[id];
      p->set_x(vertices_iter->point().x());
      p->set_y(vertices_iter->point().y());
    }
    BOOST_ASSERT(points->size() == next_id);
    BOOST_ASSERT(unused_indices.empty());
  }

  void InsertConstraintsForPolygon(const Triangulation::Polygon& polygon) {
    for (int i = 0; i < polygon.point_indices_size(); ++i) {
      int prev_idx = (i == 0) ? polygon.point_indices_size() - 1 : i - 1;
      int cur = polygon.point_indices(i);
      int prev = polygon.point_indices(prev_idx);
      cdt.insert_constraint(vertex_handles[prev], vertex_handles[cur]);
    }
  }
};

// See comments below for DelaunayTriangulationAlgorithm::Criteria::Is_bad
struct DelaunayTriangulationAlgorithm::Criteria
    : public CGAL::Delaunay_mesh_criteria_2<DelaunayTriangulationAlgorithm::CDT> {
  typedef CGAL::Delaunay_mesh_criteria_2<CDT> Base;
  typedef CGAL::Delaunay_mesh_size_criteria_2<CDT>::Quality Quality;
  typedef CDT::Geom_traits Geom_traits;
  typedef Geom_traits::Triangle_2 Triangle_2;
  typedef Geom_traits::Iso_rectangle_2 Iso_rectangle_2;
  using ConstrainedCriteria = std::vector<TriangulationParams::Criterion>;

  explicit Criteria(const TriangulationParams &triangulationParams, const Geom_traits &traits = Geom_traits())
      : Base(triangulationParams.min_aspect_bound(), traits) {
    constrained_criteria_.reserve(triangulationParams.criteria_size());
    for (const auto &constrained_criterion : triangulationParams.criteria()) {
      constrained_criteria_.push_back(constrained_criterion);
    }
  }

  class Is_bad;

  Is_bad is_bad_object() const;

 private:
  ConstrainedCriteria constrained_criteria_;
};

/*
 * This class is responsible for determining whether a triangle in the mesh must be split into several more triangles.
 * There are 2 main criteria:
 * 1) Triangle size (which is the length of its longest edge). Size constraints may be different in different
 *    regions (as defined by Criterion proto).
 * 2) Aspect ratio. It is defined as lower bound for sin²(smallest angle).
 *    Currently this criterion may be only set for the whole meshing domain.
 *    It's not recommended to use values above 0.125 for it, because otherwise meshing algorithm may diverge.
 *    For more details see the link below:
 *    https://doc.cgal.org/latest/Mesh_2/index.html#title6
 */
class DelaunayTriangulationAlgorithm::Criteria::Is_bad : Base::Is_bad {
 public:
  typedef CGAL::Mesh_2::Face_badness Face_badness;

  Is_bad(const double aspect_bound,
         const ConstrainedCriteria &criteria_list,
         const Geom_traits &traits)
      : Base::Is_bad(aspect_bound, traits),
        constrained_criteria_(criteria_list) {
  }

  Face_badness operator()(const Quality q) const {
    if (q.size() > 1)
      return Face_badness::IMPERATIVELY_BAD;
    if (q.sine() < this->B)
      return Face_badness::BAD;
    else
      return Face_badness::NOT_BAD;
  }

  Face_badness operator()(const typename CDT::Face_handle &fh, Quality &q) const {
    typedef CDT::Geom_traits::Compute_squared_distance_2 Compute_squared_distance_2;

    Compute_squared_distance_2 squared_distance = traits.compute_squared_distance_2_object();

    const Point_2 &pa = fh->vertex(0)->point();
    const Point_2 &pb = fh->vertex(1)->point();
    const Point_2 &pc = fh->vertex(2)->point();

    std::array<double, 3> sides = {
        CGAL::to_double(squared_distance(pb, pc)),
        CGAL::to_double(squared_distance(pc, pa)),
        CGAL::to_double(squared_distance(pa, pb))
    };
    std::sort(sides.begin(), sides.end());
    Triangle_2 abc = Triangle_2(pa, pb, pc);

    if (1.0 < (q.second = CheckSizeCriteria(abc, sides[2]))) {
      q.first = 1.0; // Don't compute sine.
      return Face_badness::IMPERATIVELY_BAD;
    }

    q.first = Sqr(2 * abc.area()) / (sides[1] * sides[2]);  // (sine)

    if (q.sine() < this->B)
      return Face_badness::BAD;
    else
      return Face_badness::NOT_BAD;
  }

 private:
  static Iso_rectangle_2 ConvertToIsoRectangle2(const Rectangle &r) {
    return {r.left(), r.top(), r.right(), r.bottom()};
  }

  double CheckSizeCriteria(const Triangle_2 &abc, double longest_side_sq) const {
    double squared_size_bound = std::numeric_limits<double>::infinity();
    for (const auto &item : constrained_criteria_) {
      if (!item.has_domain() || CGAL::do_intersect(abc, ConvertToIsoRectangle2(item.domain()))) {
        squared_size_bound = std::min(
            squared_size_bound,
            Sqr(item.max_side_length()));
      }
    }
    if (std::isfinite(squared_size_bound)) {
      return longest_side_sq / squared_size_bound;
    }
    return 0.0;
  }

  const ConstrainedCriteria &constrained_criteria_;
};

DelaunayTriangulationAlgorithm::Criteria::Is_bad DelaunayTriangulationAlgorithm::Criteria::is_bad_object() const {
  return {this->bound(), this->constrained_criteria_, this->traits};
}


// This class is responsible for finding grid cell where the given triangle fits.
struct DelaunayTriangulationAlgorithm::GridLocator {
 private:
  using K = CGAL::Exact_predicates_exact_constructions_kernel;
  using Traits_2 = CGAL::Arr_polyline_traits_2<CGAL::Arr_segment_traits_2<K>>;
  using Point_2 = Traits_2::Point_2;
  using Dcel = CGAL::Arr_face_extended_dcel<Traits_2, boost::optional<Triangulation::Grid::CellId>>;
  using Arr = CGAL::Arrangement_2<Traits_2, Dcel>;
 public:
  explicit GridLocator(const Triangulation &triangulation) {
    std::vector<std::pair<Point_2, Triangulation::Grid::CellId>> inner_point_to_id;
    for (const auto& cell: triangulation.grid().cells()) {
      Traits_2::Curve_2 polyline = PolygonFromIndices(cell.border(), triangulation);
      CGAL::insert(arr_, polyline);
      if (!cell.has_id())
        continue;

      auto c = GetPointInside(polyline);
      inner_point_to_id.emplace_back(c, cell.id());
    }
    point_location_.attach(arr_);
    for (const auto& point_and_id: inner_point_to_id) {
      auto location_result = point_location_.locate(point_and_id.first);
      const Arr::Face_const_handle* f = boost::get<Arr::Face_const_handle>(&location_result);
      CGAL_assertion(f != nullptr);
      arr_.non_const_handle(*f)->set_data(point_and_id.second);
    }
  }

  boost::optional<Triangulation::Grid::CellId> GetCellForPoint(double x, double y) const {
    Point_2 point(x, y);
    auto location_result = point_location_.locate(point);
    const Arr::Face_const_handle* f = boost::get<Arr::Face_const_handle>(&location_result);
    CGAL_assertion(f != nullptr);
    return (*f)->data();
  }

 private:

  static Point_2 GetPointInside(const Traits_2::Curve_2& curve) {
    CGAL::Polygon_2<K> polygon(curve.points_begin(), std::prev(curve.points_end()));
    K::Vector_2 c(0.0, 0.0);
    for (auto it = polygon.vertices_begin(); it != polygon.vertices_end(); ++it) {
      Traits_2::Point_2 point = *it;
      c = c + (point - Point_2(0.0, 0.0));
    }
    c = c * (1.0 / polygon.size());
    auto res = Point_2(0.0, 0.0) + c;
    CGAL_assertion(polygon.has_on_bounded_side(res));
    return res;
  }

  Traits_2::Curve_2 PolygonFromIndices(const Triangulation::Polygon &p,
                                       const Triangulation &triangulation) const {
    auto construct_polyline = arr_.traits()->construct_curve_2_object();
    auto extend_polyline = arr_.traits()->push_back_2_object();
    std::vector<Point_2> boundary_points;
    boundary_points.reserve(p.point_indices_size());
    for (int index: p.point_indices()) {
      const auto &point = triangulation.points(index);
      boundary_points.emplace_back(point.x(), point.y());
    }
    CGAL_assertion(boundary_points.size() > 2);
    auto polyline = construct_polyline(boundary_points.back(), boundary_points.front());
    for (size_t i = 1; i < boundary_points.size(); ++i) {
      extend_polyline(polyline, boundary_points[i]);
    }
    return polyline;
  }



  Arr arr_;
  CGAL::Arr_landmarks_point_location<Arr> point_location_;
};

void DelaunayTriangulationAlgorithm::Do(Triangulation *triangulation, const TriangulationParams &params) const {
  static const CDT::Point INFINITE_POINT = {1e100, 1e100};  // Should be still finite.
  Context context;

  context.RegisterPoints(triangulation->points());
  for (const auto &polygonBoundaries : triangulation->boundaries()) {
    context.InsertConstraintsForPolygon(polygonBoundaries);
  }
  for (const auto &cell : triangulation->grid().cells()) {
    context.InsertConstraintsForPolygon(cell.border());
  }
  GridLocator grid_locator(*triangulation);
  CGAL::Delaunay_mesher_2<CDT, Criteria> mesher(context.cdt, Criteria(params));
  std::vector<CDT::Point> seeds = {INFINITE_POINT};
  mesher.set_seeds(seeds.begin(), seeds.end());
  mesher.refine_mesh();
  CGAL::lloyd_optimize_mesh_2(context.cdt, CGAL::parameters::max_iteration_number = params.lloyd_optimization_steps());
  context.ExportAddedPoints(triangulation->mutable_points());
  for (auto it = context.cdt.finite_faces_begin(); it != context.cdt.finite_faces_end(); ++it) {
    if (!it->is_in_domain())
      continue;
    auto *triangle = triangulation->add_triangles();
    double sx = 0, sy = 0;
    for (int i = 0; i < 3; ++i) {
      Vertex_handle vh = it->vertex(i);
      triangle->mutable_border()->add_point_indices(context.GetVertexId(vh));
      sx += vh->point().x();
      sy += vh->point().y();
    }
    sx /= 3.0;
    sy /= 3.0;
    auto maybe_cell_id = grid_locator.GetCellForPoint(sx, sy);
    if (maybe_cell_id) {
      *triangle->mutable_cell_id() = *maybe_cell_id;
    }
  }
}

}  // namespace internal
}  // namespace lib2d
}  // namespace meshgen
