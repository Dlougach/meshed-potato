#include "triangulation_areas.h"

#include <CGAL/Arr_default_overlay_traits.h>
#include <CGAL/Arr_overlay_2.h>
#include <CGAL/Arr_landmarks_point_location.h>
#include <CGAL/Polygon_2.h>

#include <memory>
#include <set>

namespace meshgen {
namespace lib2d {
namespace internal {

void TriangulationAreas::ProduceOverlay() {
  struct CombineFaceData {
    FaceData operator()(bool triangulatable, const boost::optional<Triangulation::Grid::CellId> &cell_id) const {
      FaceData result;
      result.triangulatable = triangulatable;
      result.cell_id = cell_id;
      return result;
    }
  };
  CGAL::Arr_face_overlay_traits<BodiesArr, GridArr, ResultArr, CombineFaceData> overlay_traits;
  CGAL::overlay(bodies_arrangement_, grid_arrangement_, overlay_arrangement_, overlay_traits);
}

namespace {

template<class Arrangement, class Container>
TriangulationAreas::Traits_2::Curve_2 PolygonFromIndices(const Container &indices,
                                                         const Arrangement &arr,
                                                         const Triangulation &triangulation) {
  using Point_2 = TriangulationAreas::Traits_2::Point_2;
  auto construct_polyline = arr.traits()->construct_curve_2_object();
  auto extend_polyline = arr.traits()->push_back_2_object();
  std::vector<Point_2> boundary_points;
  boundary_points.reserve(indices.size());
  for (int index: indices) {
    const auto &p = triangulation.points(index);
    boundary_points.emplace_back(p.x(), p.y());
  }
  CGAL_assertion(boundary_points.size() > 2);
  auto polyline = construct_polyline(boundary_points.back(), boundary_points.front());
  for (size_t i = 1; i < boundary_points.size(); ++i) {
    extend_polyline(polyline, boundary_points[i]);
  }
  return polyline;
}

template<class K = TriangulationAreas::Traits_2::Segment_traits_2::Kernel>
inline CGAL::Polygon_2<K> AsPolygon(const TriangulationAreas::Traits_2::Curve_2 &polygon) {
  auto end = polygon.points_end();
  --end; // Remove last point (that is equal to the first anyway).
  return CGAL::Polygon_2<K>(polygon.points_begin(), end);
}

}  // namespace

void TriangulationAreas::ImportGrid(const Triangulation &triangulation) {
  std::vector<std::pair<Point_2, Triangulation::Grid::CellId>> inner_point_to_id;
  for (const auto& cell: triangulation.grid().cells()) {
    Traits_2::Curve_2 polyline = PolygonFromIndices(cell.border().point_indices(), grid_arrangement_, triangulation);
    CGAL::insert(grid_arrangement_, polyline);
    if (!cell.has_id())
      continue;
    auto polygon = AsPolygon(polyline);
    double cx = 0.0, cy = 0.0;
    for (auto it = polygon.vertices_begin(); it != polygon.vertices_end(); ++it) {
      Traits_2::Point_2 point = *it;
      cx += point.x();
      cy += point.y();
    }
    cx /= polygon.size();
    cy /= polygon.size();
    Point_2 c(cx, cy);
    CGAL_assertion(polygon.has_on_bounded_side(c));
    inner_point_to_id.emplace_back(c, cell.id());
  }
  CGAL::Arr_landmarks_point_location<GridArr> point_location;
  point_location.attach(grid_arrangement_);
  for (const auto& point_and_id: inner_point_to_id) {
    auto location_result = point_location.locate(point_and_id.first);
    const GridArr::Face_const_handle* f = boost::get<GridArr::Face_const_handle>(&location_result);
    CGAL_assertion(f != nullptr);
    grid_arrangement_.non_const_handle(*f)->set_data(point_and_id.second);
  }
}

void TriangulationAreas::ImportBodies(const Triangulation &triangulation) {
  std::set<BodiesArr::Curve_handle> polygon_handles;
  for (const auto &boundary: triangulation.boundaries()) {
    Traits_2::Curve_2 polyline = PolygonFromIndices(boundary.point_indices(), bodies_arrangement_, triangulation);
    auto handle = CGAL::insert(bodies_arrangement_, polyline);
    if (AsPolygon(polyline).is_counterclockwise_oriented()) {
      polygon_handles.insert(handle);
    }
  }
  for (auto it = bodies_arrangement_.faces_begin(); it != bodies_arrangement_.faces_end(); ++it) {
    it->set_data(false);
    if (it->is_unbounded())
      continue;
    auto ccb = it->outer_ccb();
    auto cur = ccb;
    do {
      auto half_edge_handle = *cur;
      for (auto oc_it = bodies_arrangement_.originating_curves_begin(cur);
           oc_it != bodies_arrangement_.originating_curves_end(cur); ++oc_it) {
        if (polygon_handles.count(oc_it)) {
          it->set_data(true);
          break;
        }
      }
      ++cur;
    } while (cur != ccb);
  }
}

void TriangulationAreas::PopulatePointIndices(const Triangulation &triangulation) {
  overlay_vertex_index_map_.attach(overlay_arrangement_);
  for (const auto& p: triangulation.points()) {
    CGAL::insert_point(overlay_arrangement_, Traits_2::Point_2(p.x(), p.y()));
  }
}

void TriangulationAreas::ExportNewPoints(Triangulation *triangulation) {
  int initial_size = triangulation->points_size();
  std::vector<Point_2> new_points;
  for (ResultArr::Vertex_handle vertex: overlay_arrangement_.vertex_handles()) {
    int idx = overlay_vertex_index_map_[vertex];
    if (idx < initial_size) {
      continue;
    }
    idx -= initial_size;
    if (idx >= (int)new_points.size()) {
      new_points.resize(idx + 1);
    }
    new_points[idx] = vertex->point();
  }
  for (auto point: new_points) {
    Point* out = triangulation->add_points();
    out->set_x(point.x());
    out->set_y(point.y());
  }
}


std::function<TriangulationAreas::FaceData(TriangulationAreas::Point_2)> TriangulationAreas::MakeLocator() {
  auto locator = std::make_shared<CGAL::Arr_landmarks_point_location<ResultArr>>();
  locator->attach(overlay_arrangement_);
  return [locator](Point_2 point) -> FaceData {
    auto location_result = locator->locate(point);
    const auto* f = boost::get<ResultArr::Face_const_handle>(&location_result);
    CGAL_assertion(f != nullptr);
    return (*f)->data();
  };
}

}  // namespace internal
}  // namespace lib2d
}  // namespace meshgen
