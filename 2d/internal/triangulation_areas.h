#pragma once

#include "internal_guard.h"

#include <2d/proto/triangulation.pb.h>

#include <boost/optional.hpp>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Arrangement_2.h>
#include <CGAL/Arrangement_with_history_2.h>
#include <CGAL/Arr_extended_dcel.h>
#include <CGAL/Arr_polyline_traits_2.h>
#include <CGAL/Arr_vertex_index_map.h>

#include <functional>

namespace meshgen {
namespace lib2d {
namespace internal {

class TriangulationAreas {
 public:
  struct FaceData {
    boost::optional<Triangulation::Grid::CellId> cell_id;
    bool triangulatable = false;
  };
  using Kernel = CGAL::Exact_predicates_inexact_constructions_kernel;
  using Traits_2 = CGAL::Arr_polyline_traits_2<CGAL::Arr_segment_traits_2<Kernel>>;
  using Point_2 = Traits_2::Point_2;

  using BodiesDcel = CGAL::Arr_face_extended_dcel<Traits_2, bool>;
  using BodiesArr = CGAL::Arrangement_with_history_2<Traits_2, BodiesDcel>;

  using GridDcel = CGAL::Arr_face_extended_dcel<Traits_2, boost::optional<Triangulation::Grid::CellId>>;
  using GridArr = CGAL::Arrangement_2<Traits_2, GridDcel>;

  using ResultDcel = CGAL::Arr_face_extended_dcel<Traits_2, FaceData>;
  using ResultArr = CGAL::Arrangement_2<Traits_2, ResultDcel>;
  using ResultVertexIndexMap = CGAL::Arr_vertex_index_map<ResultArr>;

  void ImportAndOverlayGridAndBodies(Triangulation* triangulation) {
    ImportGrid(*triangulation);
    ImportBodies(*triangulation);
    PopulatePointIndices(*triangulation);
    ProduceOverlay();
    ExportNewPoints(triangulation);
  }

  void ImportGrid(const Triangulation& triangulation);
  void ImportBodies(const Triangulation& triangulation);
  void PopulatePointIndices(const Triangulation& triangulation);
  void ProduceOverlay();
  void ExportNewPoints(Triangulation* triangulation);

  std::function<FaceData(Point_2)> MakeLocator();

  const GridArr& grid_arrangement() const { return grid_arrangement_; }
  const BodiesArr& bodies_arrangement() const { return bodies_arrangement_; }
  const ResultArr& overlay_arrangement() const { return overlay_arrangement_; }

 private:
  GridArr grid_arrangement_;
  BodiesArr bodies_arrangement_;
  ResultArr overlay_arrangement_;
  ResultVertexIndexMap overlay_vertex_index_map_;
};

}  // namespace internal
}  // namespace lib2d
}  // namespace meshgen
