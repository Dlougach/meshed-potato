#pragma once

#ifndef LIB2D_INTERNAL_ALLOWED
#error "You have included 2d/internal/ header into your client code. Don't do that!"
#endif

#ifdef _MSC_VER
#define DISABLE_3RD_PARTY_WARNINGS \
  __pragma(warning( push, 1 )) \
  __pragma(warning(disable: 4245 4456 4457 4458 4702))
#define ENABLE_3RD_PARTY_WARNINGS \
  __pragma(warning(pop))
#else
#define DISABLE_3RD_PARTY_WARNINGS
#define ENABLE_3RD_PARTY_WARNINGS
#endif
