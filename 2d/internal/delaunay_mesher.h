#pragma once
#include "internal_guard.h"
#include "../delaunay.h"

DISABLE_3RD_PARTY_WARNINGS
#include <boost/assert.hpp>
#include <boost/optional.hpp>
#include <boost/unordered_set.hpp>

#include <CGAL/Boolean_set_operations_2.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Constrained_triangulation_plus_2.h>
#include <CGAL/Delaunay_mesher_2.h>
#include <CGAL/Delaunay_mesh_face_base_2.h>
#include <CGAL/Delaunay_mesh_criteria_2.h>
#include <CGAL/Delaunay_mesh_size_criteria_2.h>
#include <CGAL/Delaunay_mesh_vertex_base_2.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/lloyd_optimize_mesh_2.h>
#include <CGAL/Mesh_2/Face_badness.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
ENABLE_3RD_PARTY_WARNINGS

#include <algorithm>
#include <cmath>
#include <limits>

namespace meshgen {
namespace lib2d {
namespace internal {

class DelaunayTriangulationAlgorithm : public TriangulationAlgorithm {
 public:
  void Do(Triangulation *triangulation, const TriangulationParams &params) const override;

 public:
  using K = CGAL::Exact_predicates_inexact_constructions_kernel;
  using Vb = CGAL::Triangulation_vertex_base_with_info_2<boost::optional<int>, K, CGAL::Delaunay_mesh_vertex_base_2<K>>;
  using Fb = CGAL::Triangulation_face_base_with_info_2<boost::optional<Triangulation::Grid::CellId>, K, CGAL::Delaunay_mesh_face_base_2<K>>;
  using Tds = CGAL::Triangulation_data_structure_2<Vb, Fb>;
  using CDT_Tag = CGAL::Exact_intersections_tag;
  using CDT = CGAL::Constrained_Delaunay_triangulation_2<K, Tds, CDT_Tag>;
  using CTplus = CGAL::Constrained_triangulation_plus_2<CDT>;
  typedef CDT::Vertex_handle Vertex_handle;

 private:
  struct Criteria;
  struct Context;
  struct GridLocator;
};

}  // namespace internal
}  // namespace lib2d
}  // namespace meshgen
