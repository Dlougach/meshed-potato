#include "point_deduplicator.h"

#include <map>

namespace meshgen {
namespace lib2d {

class PointsCounter::Impl {
 public:
  explicit Impl(::google::protobuf::RepeatedPtrField<Point>* points, double eps)
  : points_(points), known_points_(PointsCompare(eps)) {
    for (int i = 0; i < points_->size(); ++i) {
      known_points_[std::make_pair((*points_).Get(i).x(), (*points_).Get(i).y())] = i;
    }
  }

  int GetIndex(const Point& p) {
    auto it = known_points_.find(std::make_pair(p.x(), p.y()));
    if (it == known_points_.end()) {
      int result = points_->size();
      *points_->Add() = p;
      known_points_[std::make_pair(p.x(), p.y())] = result;
      return result;
    } else {
      return it->second;
    }
  }

 private:
  ::google::protobuf::RepeatedPtrField<Point>* points_;

  struct PointsCompare {
    PointsCompare(double eps)
     : eps(eps) {}

    double eps;

    bool operator()(const std::pair<double, double>& left, const std::pair<double, double>& right) const {
      if (left.first < right.first - eps) {
        return true;
      }
      if (left.first > right.first + eps) {
        return false;
      }
      return (left.second < right.second - eps);
    }
  };

  std::map<std::pair<double, double>, int, PointsCompare> known_points_;
};

PointsCounter::PointsCounter(::google::protobuf::RepeatedPtrField<Point> *points):
 impl_(new Impl(points, 1e-9))
{}

PointsCounter::~PointsCounter() = default;

int PointsCounter::GetIndex(const Point &p) {
  return impl_->GetIndex(p);
}

}  // namespace lib2d
}  // namespace meshgen
