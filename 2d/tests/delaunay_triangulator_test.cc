#include <2d/delaunay.h>
#include <2d/test_utils/matchers.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <google/protobuf/text_format.h>

#include <cmath>

namespace meshgen {
namespace lib2d {

using ::google::protobuf::TextFormat;
using ::testing::UnorderedElementsAre;
using ::testing::Each;
using ::testing::Eq;
using ::testing::ResultOf;
using ::testing::_;
using ::testing::ElementsAreArray;
using ::testing::SizeIs;
using ::testing::Ge;

using PointsList = std::vector<std::pair<double, double>>;

inline PointsList ToPointsList(const google::protobuf::RepeatedPtrField<Point> &points) {
  PointsList result;
  for (const auto &p : points) {
    result.push_back({p.x(), p.y()});
  }
  return result;
}

TEST(DelaunayTriangulatorTest, TestTriangulationWithoutConstraints) {
  auto triangulator = CreateDelaunayTriangulator();
  Triangulation tr;
  TextFormat::ParseFromString(R"(
        points <
          x: 0
          y: 0
        >
        points <
          x: 2
          y: 0
        >
        points <
          x: 1
          y: 2
        >
        points <
          x: 1
          y: -2
        >
        boundaries <
          point_indices: 0
          point_indices: 2
          point_indices: 1
          point_indices: 3
        >
        )", &tr);
  auto initial_points = ToPointsList(tr.points());
  TriangulationParams tr_params;
  triangulator->Do(&tr, tr_params);

  EXPECT_THAT(ToPointsList(tr.points()), PrefixIsArray(initial_points));

  std::vector<std::vector<int>> resulting_triangles;
  for (const auto &triangle : tr.triangles()) {
    resulting_triangles.emplace_back(triangle.border().point_indices().begin(),
                                     triangle.border().point_indices().end());
  }
  EXPECT_THAT(
      resulting_triangles,
      UnorderedElementsAre(
          UnorderedElementsAre(0, 1, 2),
          UnorderedElementsAre(0, 1, 3)));
}

TEST(DelaunayTriangulatorTest, TestSingleSizeCriterion) {
  auto triangulator = CreateDelaunayTriangulator();
  Triangulation tr;
  TextFormat::ParseFromString(R"(
        points < x: 0 y: 0 >
        points < x: 30 y: 0 >
        points < x: 30 y: 30 >
        points < x: 0 y: 30 >
        boundaries <
          point_indices: 0
          point_indices: 1
          point_indices: 2
          point_indices: 3
        >
        )", &tr);
  auto initial_points = ToPointsList(tr.points());
  TriangulationParams tr_params;
  TextFormat::ParseFromString(R"(
        criteria <
          domain <
            top: -1
            bottom: 31
            left: -1
            right: 31
          >
          max_side_length: 10
        >
        )", &tr_params);
  triangulator->Do(&tr, tr_params);

  PointsList resulting_points = ToPointsList(tr.points());
  // Check that first 4 elements are the same as they were.
  EXPECT_THAT(resulting_points, PrefixIsArray(initial_points));

  // Check that all triangles don't exceed the required size and that all vertices are used in the triangulation.
  auto triangle_is_small_enough = [](const PointsList &triangle) {
    for (int i = 0; i < 3; ++i) {
      auto cur = triangle[i];
      auto next = triangle[(i + 1) % 3];
      if (std::hypot(cur.first - next.first, cur.second - next.second) > 10.0) {
        return false;
      }
    }
    return true;
  };
  std::vector<bool> indexIsUsed(resulting_points.size());
  for (const auto &triangle: tr.triangles()) {
    PointsList points(3);
    for (int i = 0; i < 3; ++i) {
      indexIsUsed[triangle.border().point_indices(i)] = true;
      points[i] = resulting_points[triangle.border().point_indices(i)];
    }
    EXPECT_THAT(points, ResultOf(triangle_is_small_enough, Eq(true)));
  }
  EXPECT_THAT(indexIsUsed, Each(Eq(true)));
}

TEST(DelaunayTriangulatorTest, Grid) {
  auto triangulator = CreateDelaunayTriangulator();
  Triangulation tr;
  ASSERT_TRUE(TextFormat::ParseFromString(R"(
        points < x: 0 y: 0 >
        points < x: 30 y: 0 >
        points < x: 30 y: 30 >
        points < x: 0 y: 30 >
        grid <
          cells <
            id <
              row_index: 5
              column_index: 7
            >
            border <
              point_indices: 0
              point_indices: 1
              point_indices: 2
              point_indices: 3
            >
          >
        >
        )", &tr));
  TriangulationParams tr_params;
  TextFormat::ParseFromString("criteria < max_side_length: 10 >", &tr_params);
  triangulator->Do(&tr, tr_params);
  int num_triangle_with_cell_id = 0;
  for (const auto &triangle: tr.triangles()) {
    if (triangle.has_cell_id()) {
      ++num_triangle_with_cell_id;
    }
    EXPECT_EQ(5, triangle.cell_id().row_index());
    EXPECT_EQ(7, triangle.cell_id().column_index());
  }
  EXPECT_EQ(tr.triangles().size(), num_triangle_with_cell_id);
}

}
}

