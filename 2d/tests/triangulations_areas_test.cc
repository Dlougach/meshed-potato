#define LIB2D_INTERNAL_ALLOWED
#include <2d/internal/triangulation_areas.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <google/protobuf/text_format.h>

namespace meshgen {
namespace lib2d {
namespace internal {

using ::google::protobuf::TextFormat;
using ::testing::Eq;

TEST(TriangulationAreasTest, ImportsSinglePolygonAsBoundary) {
  Triangulation tr;
  TextFormat::ParseFromString(R"(
        points < x: 0 y: 0 >
        points < x: 2 y: 0 >
        points < x: 1 y: 2 >
        points < x: 1 y: -2 >

        boundaries <
          point_indices: 0
          point_indices: 3
          point_indices: 1
          point_indices: 2
        >
        )", &tr);
  TriangulationAreas triangulation_areas;
  triangulation_areas.ImportBodies(tr);
  const auto &arr = triangulation_areas.bodies_arrangement();
  EXPECT_THAT(arr.number_of_faces(), Eq(2));
  int num_faces_to_triangulate = 0;
  for (auto it = arr.faces_begin(); it != arr.faces_end(); ++it) {
    if (it->data())
      ++num_faces_to_triangulate;
  }
  EXPECT_THAT(num_faces_to_triangulate, Eq(1));
}

TEST(TriangulationAreasTest, IgnoresClockwiseBoundaries) {
  Triangulation tr;
  TextFormat::ParseFromString(R"(
        points < x: 0 y: 0 >
        points < x: 2 y: 0 >
        points < x: 1 y: 2 >
        points < x: 1 y: -2 >

        boundaries <
          point_indices: 0
          point_indices: 2
          point_indices: 1
          point_indices: 3
        >
        )", &tr);
  TriangulationAreas triangulation_areas;
  triangulation_areas.ImportBodies(tr);
  const auto &arr = triangulation_areas.bodies_arrangement();
  EXPECT_THAT(arr.number_of_faces(), Eq(2));
  int num_faces_to_triangulate = 0;
  for (auto it = arr.faces_begin(); it != arr.faces_end(); ++it) {
    if (it->data())
      ++num_faces_to_triangulate;
  }
  EXPECT_THAT(num_faces_to_triangulate, Eq(0));
}

TEST(TriangulationAreasTest, TwoIsolatedComponents) {
  Triangulation tr;
  /*
   *   2   4
   *   |\  |\
   *   | \ | \
   *   |  \|  \
   *   0-->1-->3
   */
  TextFormat::ParseFromString(R"(
        points < x: 0 y: 0 >
        points < x: 1 y: 0 >
        points < x: 0 y: 1 >
        points < x: 2 y: 0 >
        points < x: 2 y: 1 >

        boundaries <
          point_indices: 0
          point_indices: 1
          point_indices: 2
        >
        boundaries <
          point_indices: 1
          point_indices: 3
          point_indices: 4
        >
        )", &tr);
  TriangulationAreas triangulation_areas;
  triangulation_areas.ImportBodies(tr);
  const auto &arr = triangulation_areas.bodies_arrangement();
  EXPECT_THAT(arr.number_of_faces(), Eq(3));
  int num_faces_to_triangulate = 0;
  for (auto it = arr.faces_begin(); it != arr.faces_end(); ++it) {
    if (it->data())
      ++num_faces_to_triangulate;
  }
  EXPECT_THAT(num_faces_to_triangulate, Eq(2));
}

TEST(TriangulationAreasTest, RingWithAHole) {
  Triangulation tr;
  /*
   *   3<--2
   *   |4>5|
   *   || ||
   *   |7-6|
   *   0---1
   */
  TextFormat::ParseFromString(R"(
        points < x: 0 y: 0 >
        points < x: 4 y: 0 >
        points < x: 4 y: 4 >
        points < x: 0 y: 4 >
        points < x: 1 y: 3 >
        points < x: 3 y: 3 >
        points < x: 3 y: 1 >
        points < x: 1 y: 1 >

        boundaries <
          point_indices: 0
          point_indices: 1
          point_indices: 2
          point_indices: 3
        >
        boundaries <
          point_indices: 4
          point_indices: 5
          point_indices: 6
          point_indices: 7
        >
        )", &tr);
  TriangulationAreas triangulation_areas;
  triangulation_areas.ImportBodies(tr);
  const auto &arr = triangulation_areas.bodies_arrangement();
  EXPECT_THAT(arr.number_of_faces(), Eq(3));
  int num_faces_to_triangulate = 0;
  for (auto it = arr.faces_begin(); it != arr.faces_end(); ++it) {
    if (it->data())
      ++num_faces_to_triangulate;
  }
  EXPECT_THAT(num_faces_to_triangulate, Eq(1));
}

TEST(TriangulationAreasTest, SquareWithOneGridCell) {
  Triangulation tr;
  /*
   *   3<--2
   *   |4>5|
   *   || ||
   *   |7-6|
   *   0---1
   */
  TextFormat::ParseFromString(R"(
        points < x: 0 y: 0 >
        points < x: 4 y: 0 >
        points < x: 4 y: 4 >
        points < x: 0 y: 4 >
        points < x: 1 y: 3 >
        points < x: 3 y: 3 >
        points < x: 3 y: 1 >
        points < x: 1 y: 1 >

        boundaries <
          point_indices: 0
          point_indices: 1
          point_indices: 2
          point_indices: 3
        >
        grid <
          cells <
            border <
              point_indices: 4
              point_indices: 5
              point_indices: 6
              point_indices: 7
            >
            id <
              row_index: 1
              column_index: 1
            >
          >
        >
        )", &tr);
  TriangulationAreas triangulation_areas;
  triangulation_areas.ImportAndOverlayGridAndBodies(&tr);
  const auto &arr = triangulation_areas.overlay_arrangement();
  EXPECT_THAT(arr.number_of_faces(), Eq(3));
  int num_faces_to_triangulate = 0;
  int num_faces_with_cell_id = 0;
  for (auto it = arr.faces_begin(); it != arr.faces_end(); ++it) {
    if (it->data().triangulatable)
      ++num_faces_to_triangulate;
    if (it->data().cell_id)
      ++num_faces_with_cell_id;
  }
  EXPECT_THAT(num_faces_to_triangulate, Eq(2));
  EXPECT_THAT(num_faces_with_cell_id, Eq(1));
}

}  // namespace internal
}  // namespace lib2d
}  // namespace meshgen