# Библиотека lib_2d

Эта библиотека содержит основную (геометрическую) логику
триангуляции.

## `common.h` / `common.cc`

Файлы `common.h` и `common.cc` содержат работу с базовыми
геометрическими примитивами: точками - и их конвертацию в protobuf.

## `delaunay.h` / `delaunay.cc`
Заголовок содержит базовый (абстрактный) класс триангуляции `TriangulationAlgorithm`.
Реализация содержит класс `DelaunayTriangulationAlgorithm`, реализующий
интерфейс `TriangulationAlgorithm` с помощью библиотеки CGAL.
