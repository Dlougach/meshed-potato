#pragma once
#include <vector>
#include <gmock/gmock.h>
#include <type_traits>

namespace meshgen {
namespace testutil_internal {

template<class C>
class PrefixIsArrayImpl {
 public:
  explicit PrefixIsArrayImpl(C element_matchers)
      : element_matchers_(std::move(element_matchers)) {}

  template<class Container>
  operator testing::Matcher<Container>() const { // NOLINT: it needs to be implicit here.
    size_t max_size = element_matchers_.size();
    auto truncate_container = [max_size](Container c) {
      using value_type = typename std::iterator_traits<decltype(std::begin(c))>::value_type;
      std::vector<value_type> result(
          std::make_move_iterator(std::begin(c)),
          std::make_move_iterator(std::end(c))
      );
      if (result.size() > max_size) {
        result.resize(max_size);
      }
      return result;
    };
    return testing::ResultOf(truncate_container, testing::ElementsAreArray(element_matchers_));
  }

  C element_matchers_;
};

} // namespace testutil_internal

template<class C>
testutil_internal::PrefixIsArrayImpl<C> PrefixIsArray(C element_matchers) {
  return testutil_internal::PrefixIsArrayImpl<C>(std::move(element_matchers));
}

} // namespace meshgen
