#pragma once

#include <2d/proto/triangulation.pb.h>

#include <memory>

namespace meshgen {
namespace lib2d {

class TriangulationAlgorithm {
 public:
  virtual ~TriangulationAlgorithm() = default;
  virtual void Do(Triangulation *triangulation, const TriangulationParams &) const = 0;
};

std::unique_ptr<TriangulationAlgorithm> CreateDelaunayTriangulator();

}  // namespace lib2d
}  // namespace meshgen
