find_package(Protobuf REQUIRED)

SET(PROTO_FILES
        primitives.proto
        triangulation.proto)
protobuf_generate_cpp(PB_SRCS PB_HDRS ${PROTO_FILES})
add_library(2d_proto ${PB_SRCS} ${PB_HDRS} ${PROTO_FILES})
target_include_directories(2d_proto PUBLIC  ${PROTOBUF_INCLUDE_DIRS})
target_include_directories(2d_proto PRIVATE ${CMAKE_CURRENT_BINARY_DIR})

if (${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
    target_compile_options(2d_proto PRIVATE "-Wno-unused-parameter" "-Wno-array-bounds")
endif ()


target_link_libraries(2d_proto ${PROTOBUF_LIBRARIES})
