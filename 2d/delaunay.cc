#include "delaunay.h"
#include "internal/delaunay_mesher.h"
#include <compat/make_unique.h>

namespace meshgen {
namespace lib2d {

std::unique_ptr<TriangulationAlgorithm> CreateDelaunayTriangulator() {
  return make_unique<internal::DelaunayTriangulationAlgorithm>();
}

}  // namespace lib2d
}  // namespace meshgen
