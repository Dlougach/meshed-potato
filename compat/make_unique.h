#pragma once

#include <memory>

namespace meshgen {

// std::make_unique only exists since C++14, and we want to support C++11.
template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args &&... args) {
  return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

}  // namespace meshgen
