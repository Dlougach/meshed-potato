file(GLOB SOURCE_FILES
        *.h
        *.cc)

add_library(compat ${SOURCE_FILES})
