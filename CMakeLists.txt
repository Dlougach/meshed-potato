cmake_minimum_required(VERSION 3.11)

project(MESHGEN)

enable_testing()

set (CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")
include(common)
include(External_GTest)
include(GoogleTest)

add_subdirectory(compat)
add_subdirectory(2d)
add_subdirectory(cli)